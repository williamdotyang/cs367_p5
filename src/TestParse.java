import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.Set;


public class TestParse {
	private static String gameIntro; // initial introduction to the game
	private static String winningMessage; //winning message of game
	private static String gameInfo; //additional game info
	private static boolean gameWon = false; //state of the game
	private static Scanner scanner = null; //for reading files
	private static Scanner ioscanner = null; //for reading standard input
	private static Player player; //object for player of the game
	private static Room location; //current room in which player is located
	private static Room winningRoom; //Room which player must reach to win
	private static Item winningItem; //Item which player must find
	private static DirectedGraph<Room> layout; //Graph structure of the Rooms

	public static void main(String[] args) {
		if (args.length != 1) {
			System.err.println("Bad invocation! Correct usage: "
					+ "java AppStore <gameFile>");
			System.exit(1);
		}

		boolean didInitialize = initializeGame(args[0]);

		if (!didInitialize) {
			System.err.println("Failed to initialize the application!");
			System.exit(1);
		}

		System.out.println("Parsing successed!" + gameIntro); // game intro		

	}

	private static boolean initializeGame(String gameFile) {

		try {

			String playerName = "Huilin";

			File inputFile = new File(gameFile);
			scanner = new Scanner(inputFile);

			gameIntro = scanner.nextLine();
			winningMessage = scanner.nextLine();
			gameInfo = scanner.nextLine();

			while (scanner.hasNextLine())	{
				if (scanner.hasNext("#player")) 
					player = ParsePlayer(scanner, playerName);

				if (scanner.hasNext("#room")) {
					Room room = null;
					if (scanner.hasNext("#win")) { 
						room = ParseRoom(scanner);
						winningRoom = room;
					} else {
						room = ParseRoom(scanner);
					}
					layout.addVertex(room);
				}

				if (scanner.hasNext("#locked passages"))
					ParseLockedPassages(scanner);

				if (scanner.hasNext("#Adjacency List"))
					ParseAdjacencyList(scanner);
				
				scanner.nextLine(); // consume the end of line
			}
			
			scanner.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (Exception e){
			e.printStackTrace();
			return false;
		}

		return true;
	}
		
	/**
	 * Handles "#item" tag
	 * @param scanner
	 * @return Item object
	 * @throws ParseException 
	 */
	private static Item ParseItem(Scanner scanner) {
		String tagLine = scanner.nextLine();
		if (!tagLine.contains("#item")) {
			throw new RuntimeException("this method is not called in the right way");
		}
		
		String name = scanner.nextLine().trim();
		String description = scanner.nextLine().trim();
		boolean activated = Boolean.parseBoolean(scanner.nextLine().trim());
		String message = scanner.nextLine().trim();
		boolean oneTimeUse = Boolean.parseBoolean(scanner.nextLine().trim());
		String usedString = scanner.nextLine().trim();
			
		Item item = new Item(name, description, activated, message, oneTimeUse, usedString);
		
		return item;
	}

	/**
	 * Handles "#player" tag
	 * @param scanner
	 * @param playerName
	 * @return The line with new tag
	 */
	private static Player ParsePlayer(Scanner scanner, String playerName) {
		String tagLine = scanner.nextLine();
		if (!tagLine.contains("#player items")) {
			System.out.println(tagLine);
			throw new RuntimeException("this method is not called in the right way");
		}
		
		Set<Item> sack = new HashSet<Item>();
		
		while(scanner.hasNext("#item")) 
			sack.add(ParseItem(scanner));
		
		player = new Player(playerName, sack);
		return player;
	}
	
	
	private static MessageHandler ParseMessagehandler(Scanner scanner) {
		String msg = scanner.nextLine().trim();
		String type = scanner.nextLine().trim();
		String newRoomName = null;
		if (type.equals("room"))
			newRoomName  = scanner.nextLine().trim();

		return new MessageHandler(msg, type, newRoomName);
	}
	
	/**
	 * Handles "#room" tag, parse one room.
	 * @param scanner2
	 * @return Room object
	 */
	private static Room ParseRoom(Scanner scanner) {
		String tagLine = scanner.nextLine();
		if (!tagLine.contains("#room")) {
			System.out.println(tagLine);
			throw new RuntimeException("this method is not called in the right way");
		}
		
				String name = scanner.nextLine();
				String description = scanner.nextLine();
				boolean visibility = Boolean.parseBoolean(scanner.nextLine().trim());
				boolean habitability = Boolean.parseBoolean(scanner.nextLine().trim());
				String habMsg = null;
				if (habitability == false)
					habMsg = scanner.nextLine();

				Set<Item> items = new HashSet<Item>();
				List<MessageHandler> handlers = new ArrayList<MessageHandler>();
				
				if (scanner.hasNext("#item")) {
					Item item = null;
					if (scanner.hasNext("#win")) {
						item = ParseItem(scanner);
						winningItem = item;
					} else {
						item = ParseItem(scanner);
					}
					items.add(item);
				}
					
				if (scanner.hasNext("#messageHandler")) {
					MessageHandler mh = ParseMessagehandler(scanner);
					handlers.add(mh);
				}

				return new Room(name, description, visibility, habitability,
						habMsg, items, handlers);
			}  
	
	/**
	 * Parse the "#locked passage" tag and add <Room, String> pair to the 
	 * corresponding room's lockedPassages.
	 * @param scanner
	 */
	private static void ParseLockedPassages(Scanner scanner) {
		String tagLine = scanner.nextLine();
		if (!tagLine.contains("#locked passage")) {
			throw new RuntimeException("this method is not called in the right way");
		}
		
		String roomName1 = scanner.next();
		String roomName2 = scanner.next();
		String reason = scanner.nextLine();
		
		Set<Room> rooms = layout.getAllVertices();
		Room room1 = findRoomFromSet(roomName1, rooms);
		Room room2 = findRoomFromSet(roomName2, rooms);;
		
		if (room1 == null || room2 == null) {
			System.err.println("The rooms are not found!");
			System.exit(-1);
		}
		room1.addLockedPassage(room2, reason);
	}
	
	/**
	 * Add neighbors to corresponding room.
	 * @param scanner
	 */
	private static void ParseAdjacencyList(Scanner scanner) {
		String tagLine = scanner.nextLine();
		if (!tagLine.contains("#Adjacency List")) {
			throw new RuntimeException("this method is not called in the right way");
		}
		
		Set<Room> rooms = layout.getAllVertices();
		String fromRoomName = scanner.next();
		Room fromRoom = findRoomFromSet(fromRoomName, rooms);
		
		while (scanner.hasNext()) {
			String toRoomName = scanner.next();
			Room toRoom = findRoomFromSet(toRoomName, rooms);
			if (toRoom != null) layout.addEdge(fromRoom, toRoom);
		}
	}
	
	/**
	 * Given a Set of rooms, look them up by name.
	 * @param roomName
	 * @param roomList
	 * @return The corresponding room object if name matches, null otherwise.
	 */
	private static Room findRoomFromSet(String roomName, Set<Room> roomList) {
		Iterator<Room> rmItr = roomList.iterator();
		while (rmItr.hasNext()) {
			Room room = rmItr.next();
			if (room.getName().equals(roomName)) return room; 
		}
		return null;
	}
	
}

