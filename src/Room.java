///////////////////////////////////////////////////////////////////////////////
//                   ALL STUDENTS COMPLETE THESE SECTIONS
// Main Class File:  TheGame.java
// File:             Room.java
// Semester:         CS367 Fall 2015
//
// Author:           Weilan Yang, wyang65@wisc.edu
// CS Login:         wyang
// Lecturer's Name:  Jim Skrentny
// Section:      	 Lecture 2
//
//////////////////// PAIR PROGRAMMERS COMPLETE THIS SECTION ////////////////////
//
// Pair Partner:     Huilin Hu
// Email:            hhu28@wisc.edu
// CS Login:         huilin
// Lecturer's Name:  Jim Skrentny
// Section:      	 Lecture 2
//
//////////////////////////// 80 columns wide //////////////////////////////////
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * The room describes where the player is located. The first room listed in 
 * the file is the starting "location" of the player.
 * Rooms have their own name and description
 * They have visibility and habitability parameters.
 * Rooms have Items in them. Duplicate items are not allowed.
 * Rooms have a List of MessageHandlers to handle messages that Items send to them.
 * A Room could have locked doors, which are locked for some reason.
 *
 * @author Weilan Yang
 */
public class Room {
	//name of the room
	private	String	name;
	//description of the room
	private	String	description;
	//whether the room is lit or dark
	private	boolean	visibility;
	//whether the room is habitable
	private	boolean habitability;
	//reason for room not being habitable (only relevant when habitability is false)
	private String	habitableMsg;
	//items in the room
	private	Set<Item>	items;
	// message handlers
	private	List<MessageHandler> handlers;
	//locked rooms and the reason for their being locked
	private HashMap<Room, String> lockedPassages;	
	//Do not add anymore data members
	
	// constructor 
	public Room(String name, String description, boolean visibility, boolean habitability,
			String habMsg, Set<Item> items, List<MessageHandler> handlers){
		if (name == null || description == null || items == null || handlers == null) 
			throw new IllegalArgumentException("Arguments cannot be null!");
		if (habitability == false && habMsg == null)
			throw new IllegalArgumentException("A room cannot be inhabitable without reasons!");
		this.name = name;
		this.description = description;
		this.visibility = visibility;
		this.habitability = habitability;
		this.habitableMsg = habMsg;
		this.items = items;
		this.handlers = handlers;
		lockedPassages = new HashMap<Room,String>();
	}
	
	// methods
	/**
	 * Getter method to get the Room's name
	 * @return name
	 */
	public String getName(){
		return name;
	}
	
	/**
	 * Returns whether the visibility in the room is true or false
	 * @return visibility
	 */
	public boolean isVisible(){
		return visibility;
	}
	
	/**
	 * Returns true if the room is habitable, otherwise returns false
	 * @return habitability
	 */
	public boolean isHabitable(){
		return habitability;
	}
	
	/**
	 * If the room is not habitable, returns a String describing the reason 
	 * why it is not habitable. If the room is habitable returns null.
	 * @return String of message, or null
	 */
	public String getHabitableMsg(){
		if (!habitability) return habitableMsg;
		return null;
	}
	
	/**
	 * Returns the HashMap containing the lockedRooms and the corresponding 
	 * reasons why they are locked.
	 * @return A HashMap object
	 */
	public HashMap<Room, String> getLockedPassages(){
		return lockedPassages;
	}
	
	/**
	 * Adds a (RoomName, ReasonWhyLocked) pair to the list of locked passages 
	 * for a room.
	 * @param passage A room object
	 * @param whyLocked A String of reason being locked
	 */
	public void addLockedPassage(Room passage, String whyLocked){
		if (passage == null || whyLocked == null) 
			throw new IllegalArgumentException("Arguments cannot be null!");
		lockedPassages.put(passage, whyLocked);
	}
	
	/**
	 * If it finds the Item "itemName" in the Room's items, it returns that 
	 * Item. Otherwise it returns null 
	 * @param item The name of the item to be looked for
	 * @return The found item object, or null
	 */
	public Item findItem(String item){
		if (item == null)
			throw new IllegalArgumentException("Arguments cannot be null!");
		
		Iterator<Item> itmItr = items.iterator();
		while(itmItr.hasNext()) {
			Item element = itmItr.next();
			if (element.getName().equals(item)) return element;
		}
		return null;
	}
	
	/**
	 * Adds an the "item" to the Room's items. Duplicate items are not allowed. 
	 * @param item An Item object
	 * @return true if item is added, false otherwise
	 */
	public boolean addItem(Item item){
		if (item == null)
			throw new IllegalArgumentException("Arguments cannot be null!");
		return items.add(item);
	}
	
	/**
	 * Removes the "item" from the Rooms Set of items.  
	 * @param item An Item object
	 * @return true if item was removed, false otherwise
	 */
	public boolean removeItem(Item item){
		if (item == null)
			throw new IllegalArgumentException("Arguments cannot be null!");
		return items.remove(item);
	}

	/***
	 * Receives messages from items used by the player and executes the 
	 * appropriate action stored in a message handler
	 * @param message is the "message" sent by the item
	 * @return null, this Room or Unlocked Room depending on MessageHandler
	 * DO NOT MODIFY THIS METHOD
	 */
	public Room receiveMessage(String message){
		Iterator<MessageHandler> itr = handlers.iterator();
		MessageHandler msg = null;
		while(itr.hasNext()){
			msg = itr.next();
			if(msg.getExpectedMessage().equalsIgnoreCase(message))
				break;
			msg = null;
		}
		if(msg == null)
			return null;
		switch(msg.getType()){
		case("visibility") :
			this.visibility = true;
			return this;
		case("habitability") :
			this.habitability = true;
			return this;
		case("room") :
			for(Room rm : this.lockedPassages.keySet()){
				if(rm.getName().equalsIgnoreCase(msg.getRoomName())){
					this.lockedPassages.remove(rm);
					return rm;
				}
			}
		default:
			return null;
		}
	}

	@Override
	//Returns a String consisting of the Rooms name, its description,
	//its items and locked rooms.
	// DO NOT MODIFY THIS METHOD
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("Welcome to the " + name + "!");
		sb.append(System.getProperty("line.separator"));
		if(!this.visibility){
			sb.append("Its too dark to see a thing!");
			sb.append(System.getProperty("line.separator"));
			sb.append("Places that can be reached from here :");
			sb.append(System.getProperty("line.separator"));
			for(Room rm :this.lockedPassages.keySet()){
				sb.append(rm.getName());
				sb.append(System.getProperty("line.separator"));
			}
			return sb.toString();
		}
		sb.append(description);
		sb.append(System.getProperty("line.separator"));
		if(this.items.size() >0){ 
			sb.append("Some things that stand out from the rest :");
			sb.append(System.getProperty("line.separator"));
		}
		Iterator<Item> itr = this.items.iterator();
		while(itr.hasNext()){
			sb.append(itr.next().getName());
			sb.append(System.getProperty("line.separator"));
		}
		sb.append("Places that can be reached from here :");
		sb.append(System.getProperty("line.separator"));
		for(Room rm :this.lockedPassages.keySet()){
			sb.append(rm.getName());
			sb.append(System.getProperty("line.separator"));
		}
		return sb.toString();
	}
}

