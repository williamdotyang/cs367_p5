///////////////////////////////////////////////////////////////////////////////
//                   ALL STUDENTS COMPLETE THESE SECTIONS
// Title:            Program 5
// Files:            Player.java, Item.java, MessageHandler.java
//					 DirectedGraph.java, Room.java, TheGame.java
//					 
// Semester:         CS 367 Fall 2015
//
// Author:           Weilan Yang
// Email:            wyang65@wisc.edu
// CS Login:         wyang
// Lecturer's Name:  Jim Skrentny
// Lab Section:      Section 2
//
//////////////////// PAIR PROGRAMMERS COMPLETE THIS SECTION ////////////////////
//
// Pair Partner:     Huilin Hu
// Email:            hhu28@wisc.edu
// CS Login:         huilin
// Lecturer's Name:  Jim Skrentny
// Lab Section:      Section 2
///////////////////////////////////////////////////////////////////////////////
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Pattern;

public class TheGame {
	private static String gameIntro; // initial introduction to the game
	private static String winningMessage; //winning message of game
	private static String gameInfo; //additional game info
	private static boolean gameWon = false; //state of the game
	private static Scanner scanner = null; //for reading files
	private static Scanner ioscanner = null; //for reading standard input
	private static Player player; //object for player of the game
	private static Room location; //current room in which player is located
	private static Room winningRoom; //Room which player must reach to win
	private static Item winningItem; //Item which player must find
	private static DirectedGraph<Room> layout; //Graph structure of the Rooms

	public static void main(String[] args) {
		if (args.length != 1) {
			System.err.println("Bad invocation! Correct usage: "
					+ "java AppStore <gameFile>");
			System.exit(1);
		}

		boolean didInitialize = initializeGame(args[0]);

		if (!didInitialize) {
			System.err.println("Failed to initialize the application!");
			System.exit(1);
		}

		System.out.println(gameIntro); // game intro

		processUserCommands();
	}

	private static boolean initializeGame(String gameFile) {

		try {

			// reads player name
			System.out.println("Welcome worthy squire! What might be your name?");
			ioscanner = new Scanner(System.in);
			String playerName = ioscanner.nextLine();
						
			File inputFile = new File(gameFile);
			scanner = new Scanner(inputFile);

			gameIntro = scanner.nextLine();
			winningMessage = scanner.nextLine();
			gameInfo = scanner.nextLine();
			
			layout = new DirectedGraph<Room>();
			location = null;
			while (scanner.hasNextLine())	{
				if (scanner.hasNext(Pattern.compile(".*#player.*"))) {
					player = parsePlayer(scanner, playerName);
					continue; // go to the next tag line
				}

				else if (scanner.hasNext(Pattern.compile(".*#room.*"))) {
					Room room = null;
					if (scanner.hasNext(Pattern.compile(".*#win.*"))) {
						room = parseRoom(scanner);
						winningRoom = room;
					} else {
						room = parseRoom(scanner);
					}
					layout.addVertex(room);
					if (location == null) location = room;
					continue; // go to the next tag line 
				}

				else if (scanner.hasNext(Pattern.compile(".*#locked.*"))) { 
					parseLockedPassages(scanner);
					continue; // go to the next tag line
				}

				else if (scanner.hasNext(Pattern.compile(".*#Adjacency.*"))) {
					parseAdjacencyList(scanner);
					continue; // go to the next tag line
				}

				else  // cannot parse this line
					throw new ParseException("cannot parse this line:"
							+ scanner.nextLine(), 0);
			}
			
			scanner.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (Exception e){
			e.printStackTrace();
			return false;
		}

		return true;
	}

	/**
	 * Handles "#item" tag
	 * @param scanner
	 * @return Item object
	 * @throws ParseException 
	 */
	private static Item parseItem(Scanner scanner) throws ParseException {
		String tagLine = scanner.nextLine();
		if (!tagLine.contains("#item")) {
			System.out.println(tagLine);
			throw new ParseException("this method is not called in the right way", 0);
		}

		String name = scanner.nextLine().trim();
		String description = scanner.nextLine().trim();
		boolean activated = Boolean.parseBoolean(scanner.nextLine().trim());
		String message = scanner.nextLine().trim();
		boolean oneTimeUse = Boolean.parseBoolean(scanner.nextLine().trim());
		String usedString = scanner.nextLine().trim();

		Item item = new Item(name, description, activated, message, oneTimeUse, usedString);

		return item;
	}

	/**
	 * Handles "#player" tag
	 * @param scanner
	 * @param playerName
	 * @return The line with new tag
	 * @throws ParseException 
	 */
	private static Player parsePlayer(Scanner scanner, String playerName) 
			throws ParseException {
		String tagLine = scanner.nextLine();
		if (!tagLine.contains("#player items")) {
			System.out.println(tagLine);
			throw new ParseException("this method is not called in the right way", 0);
		}

		Set<Item> sack = new HashSet<Item>();

		while(scanner.hasNext(Pattern.compile(".*#item.*")))
			sack.add(parseItem(scanner));

		Player player = new Player(playerName, sack);
		return player;
	}

	/**
	 * Handles "#MessageHandler" tag
	 * @param scanner
	 * @return
	 * @throws ParseException 
	 */
	private static MessageHandler parseMessagehandler(Scanner scanner) throws ParseException {
		String tagLine = scanner.nextLine();
		if (!tagLine.contains("#messageHandler")) {
			System.out.println(tagLine);
			throw new ParseException("this method is not called in the right way", 0);
		}
		String msg = scanner.nextLine().trim();
		String type = scanner.nextLine().trim();
		String newRoomName = null;
		if (type.equals("room"))
			newRoomName  = scanner.nextLine().trim();

		return new MessageHandler(msg, type, newRoomName);
	}

	/**
	 * Handles "#room" tag, parse one room.
	 * @param scanner2
	 * @return Room object
	 * @throws ParseException 
	 */
	private static Room parseRoom(Scanner scanner) throws ParseException {
		String tagLine = scanner.nextLine();
		if (!tagLine.contains("#room")) {
			System.out.println(tagLine);
			throw new ParseException("this method is not called in the right way", 0);
		}

		String name = scanner.nextLine().trim();
		String description = scanner.nextLine().trim();
		boolean visibility = Boolean.parseBoolean(scanner.nextLine().trim());
		boolean habitability = Boolean.parseBoolean(scanner.nextLine().trim());
		String habMsg = null;
		if (habitability == false)
			habMsg = scanner.nextLine().trim();

		Set<Item> items = new HashSet<Item>();
		List<MessageHandler> handlers = new ArrayList<MessageHandler>();

		while (scanner.hasNext(Pattern.compile(".*#item.*"))) {
			Item item = null;
			if (scanner.hasNext(Pattern.compile(".*#win.*"))) {
				item = parseItem(scanner);
				winningItem = item;
			} else {
				item = parseItem(scanner);
			}
			items.add(item);
		}
		
		while (scanner.hasNext(Pattern.compile(".*#messageHandler.*"))) {
			MessageHandler mh = parseMessagehandler(scanner);
			handlers.add(mh);
		}

		return new Room(name, description, visibility, habitability,
				habMsg, items, handlers);
	}  

	/**
	 * parse the "#locked passage" tag and add <Room, String> pair to the 
	 * corresponding room's lockedPassages.
	 * @param scanner
	 * @throws ParseException 
	 */
	private static void parseLockedPassages(Scanner scanner) throws ParseException {
		String tagLine = scanner.nextLine();
		if (!tagLine.contains("#locked passage")) {
			System.out.println(tagLine);
			throw new ParseException("this method is not called in the right way", 0);
		}

		while (!scanner.hasNext(Pattern.compile(".*#.*"))) {
			String roomName1 = scanner.next().trim();
			String roomName2 = scanner.nextLine().trim();
			String reason = scanner.nextLine().trim();

			Set<Room> rooms = layout.getAllVertices();
			Room room1 = findRoomFromSet(roomName1, rooms);
			Room room2 = findRoomFromSet(roomName2, rooms);

			if (room1 == null || room2 == null) 
				throw new ParseException("The rooms are not found!", 0);

			room1.addLockedPassage(room2, reason);
		}
	}

	/**
	 * Add neighbors to corresponding room.
	 * @param scanner
	 * @throws ParseException 
	 */
	private static void parseAdjacencyList(Scanner scanner) throws ParseException {
		String tagLine = scanner.nextLine();
		if (!tagLine.contains("#Adjacency List")) {
			System.out.println(tagLine);
			throw new ParseException("this method is not called in the right way", 0);
		}

		Set<Room> rooms = layout.getAllVertices();
		
		while (scanner.hasNextLine()) { // while in this block
			String[] roomNames = scanner.nextLine().split(" ");
			String fromRoomName = roomNames[0]; // first name is the from
			Room fromRoom = findRoomFromSet(fromRoomName, rooms);
			for (int i = 1; i < roomNames.length; i++) { // the following names are to
				String toRoomName = roomNames[i];
				Room toRoom = findRoomFromSet(toRoomName, rooms);
				layout.addEdge(fromRoom, toRoom);
			}
		}
	}

	/**
	 * Given a Set of rooms, look them up by name.
	 * @param roomName
	 * @param roomList
	 * @return The corresponding room object if name matches, null otherwise.
	 */
	private static Room findRoomFromSet(String roomName, Set<Room> roomList) {
		Iterator<Room> rmItr = roomList.iterator();
		while (rmItr.hasNext()) {
			Room room = rmItr.next();
			if (room.getName().equals(roomName)) return room; 
		}
		return null;
	}

	private static void processUserCommands() {
		String command = null;
		do {

			System.out.print("\nPlease Enter a command ([H]elp):");
			command = ioscanner.next();
			switch (command.toLowerCase()) {
			case "p": // pick up
				processPickUp(ioscanner.nextLine().trim());
				goalStateReached();
				break;
			case "d": // put down item
				processPutDown(ioscanner.nextLine().trim());
				break;
			case "u": // use item
				processUse(ioscanner.nextLine().trim());
				break;
			case "lr":// look around
				processLookAround();
				break;
			case "lt":// look at
				processLookAt(ioscanner.nextLine().trim());
				break;
			case "ls":// look at sack
				System.out.println(player.printSack());
				break;
			case "g":// goto room
				processGoTo(ioscanner.nextLine().trim());
				goalStateReached();
				break;
			case "q":
				System.out.println("You Quit! You, " + player.getName() + ", are a loser!!");
				break;
			case "i":
				System.out.println(gameInfo);
				break;
			case "h":
				System.out
				.println("\nCommands are indicated in [], and may be followed by \n"+
						"any additional information which may be needed, indicated within <>.\n"
						+ "\t[p]  pick up item: <item name>\n"
						+ "\t[d]  put down item: <item name>\n"
						+ "\t[u]  use item: <item name>\n"
						+ "\t[lr] look around\n"
						+ "\t[lt] look at item: <item name>\n"
						+ "\t[ls] look in your magic sack\n"
						+ "\t[g]  go to: <destination name>\n"
						+ "\t[q]  quit\n"
						+ "\t[i]  game info\n");
				break;
			default:
				System.out.println("Unrecognized Command!");
				break;
			}
		} while (!command.equalsIgnoreCase("q") && !gameWon);
		ioscanner.close();
	}

	private static void processLookAround() {
		System.out.print(location.toString());
		for(Room rm : layout.getNeighbors(location)){
			System.out.println(rm.getName());
		}
	}

	private static void processLookAt(String item) {
		Item itm = player.findItem(item);
		if(itm == null){
			itm = location.findItem(item);
		}
		if(itm == null){
			System.out.println(item + " not found");
		}
		else
			System.out.println(itm.toString());
	}

	private static void processPickUp(String item) {
		if(player.findItem(item) != null){
			System.out.println(item + " already in sack");
			return;
		}
		Item newItem = location.findItem(item);
		if(newItem == null){
			System.out.println("Could not find " + item);
			return;
		}
		player.addItem(newItem);
		location.removeItem(newItem);
		System.out.println("You picked up ");
		System.out.println(newItem.toString());
	}

	private static void processPutDown(String item) {
		if(player.findItem(item) == null){
			System.out.println(item + " not in sack");
			return;
		}
		Item newItem = player.findItem(item);
		location.addItem(newItem);
		player.removeItem(newItem);
		System.out.println("You put down " + item);
	}

	private static void processUse(String item) {
		Item newItem = player.findItem(item);
		if(newItem == null){
			System.out.println("Your magic sack doesn't have a " + item);
			return;
		}
		if (newItem.activated()) {
			System.out.println(item + " already in use");
			return;
		}
		if(notifyRoom(newItem)){
			if (newItem.isOneTimeUse()) {
				player.removeItem(newItem);
			}
		}
	}

	private static void processGoTo(String destination) {
		Room dest = findRoomInNeighbours(destination);
		if(dest == null) {
			for(Room rm : location.getLockedPassages().keySet()){
				if(rm.getName().equalsIgnoreCase(destination)){
					System.out.println(location.getLockedPassages().get(rm));
					return;
				}
			}
			System.out.println("Cannot go to " + destination + " from here");
			return;
		}
		Room prevLoc = location;
		location = dest;
		if(!player.getActiveItems().isEmpty())
			System.out.println("The following items are active:");
		for(Item itm:player.getActiveItems()){
			notifyRoom(itm);
		}
		if(!dest.isHabitable()){
			System.out.println("Thou shall not pass because");
			System.out.println(dest.getHabitableMsg());
			location = prevLoc;
			return;
		}

		System.out.println();
		processLookAround();
	}

	private static boolean notifyRoom(Item item) {
		Room toUnlock = location.receiveMessage(item.on_use());
		if (toUnlock == null) {
			if(!item.activated())
				System.out.println("The " + item.getName() + " cannot be used here");
			return false;
		} else if (toUnlock == location) {
			System.out.println(item.getName() + ": " + item.on_useString());
			item.activate();
		} else {
			// add edge from location to to Unlock
			layout.addEdge(location, toUnlock);
			if(!item.activated())
				System.out.println(item.on_useString());
			item.activate();
		}
		return true;
	}

	private static Room findRoomInNeighbours(String room) {
		Set<Room> neighbours = layout.getNeighbors(location);
		for(Room rm : neighbours){
			if(rm.getName().equalsIgnoreCase(room)){
				return rm;
			}
		}
		return null;
	}

	private static void goalStateReached() {
		if ((location == winningRoom && player.hasItem(winningItem)) 
				|| (location == winningRoom && winningItem == null)){
			System.out.println("Congratulations, " + player.getName() + "!");
			System.out.println(winningMessage);
			System.out.println(gameInfo);
			gameWon = true;
		}
	}

}
