import java.util.Set;
//import java.util.Iterator;
import java.util.HashSet;

public class TestPlayer {
	public static void main(String[] args) {
	// correct case
	Set <Item> set1 = new HashSet <Item> ();
	
	Item item1 = new Item("Flashlight", "A shiny 2100 lumens torch", true, "flashlight on",
			false, "The flashlight is now on");
	// item1 and item2 are same: test set class
	Item item2 = new Item("Flashlight", "A shiny 2100 lumens torch", true, "flashlight on",
			false, "The flashlight is now on");
	Item item3 = new Item("Key", "Old and rusty key to an unknown door (oooooh!", false, 
			"key to barn has been used", true, "The barn is now open");
	Item item4 = new Item("Car Key", "Toyota ignition key", false, "no message", false,
			"You can't actually use this");
	Item item5 = new Item("Dog","his name is barky", true, "bow wow", false,
			"You cannot use a dog, you strange person!");

	/*
	set1.add(item1);
	System.out.println(item1);
	set1.add(item2);
	System.out.println(set1.size());
	
	*/
	
	set1.add(item1);
	set1.add(item2);
	set1.add(item3);
	set1.add(item4);
	set1.add(item5);
	System.out.println(set1.size());
	
	System.out.println("total number of items " + set1.size());
	
	Item item6 = new Item("Cat","his name is minnie", true, "bow", false,
			"You cannot use a cat");
	
	Item item7 = new Item("Flashlight", "A shiny 2100 lumens torch", true, "flashlight on",
			false, "The flashlight is now on");
	
	Player player1 = new Player("Huilin", set1);
	System.out.println(player1.getName());
	//System.out.println(player1.printSack());
	System.out.println("number of active items "  + player1.getActiveItems().size());
	
	System.out.println(player1.findItem("Key"));
	System.out.println(player1.hasItem(item3));
	System.out.println(player1.hasItem(item6));
	
	
	System.out.println("Can be added? " + player1.addItem(item7));
	
	System.out.println(set1.size());
	System.out.println("number of items 2: "  + player1.getActiveItems().size());
	
	System.out.println(player1.removeItem(item7));
	System.out.println("number of items 3: "  + player1.getActiveItems().size());
	
	
	
	}

}
