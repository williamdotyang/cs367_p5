public class TestItem {

	public static void main(String[] args) {
	// correct case
	
	Item item1 = new Item("Flashlight", "A shiny 2100 lumens torch", true, "flashlight on",
			false, "The flashlight is now on");
	System.out.println(item1.getName());
	System.out.println(item1.getDescription());
	System.out.println(item1.isOneTimeUse());
	System.out.println(item1.on_useString());
	System.out.println(item1.on_use());
	System.out.println(item1.activated());
	item1.activate();
	System.out.println(item1.activated());
	
	System.out.println("test toString() " + item1.toString());
	
	// exception: all exceptions have been tested
	Item item2 = new Item("Flashlight", "A shiny 2100 lumens torch", false, "flashlight on",
			false, null);
	
	System.out.println(item2.getName());
	System.out.println(item2.getDescription());
	System.out.println(item2.isOneTimeUse());
	System.out.println(item2.on_useString());
	System.out.println(item2.on_use());
	System.out.println(item2.activated());
	item2.activate();
	System.out.println(item2.activated());

	
	}

}
