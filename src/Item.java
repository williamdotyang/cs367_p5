///////////////////////////////////////////////////////////////////////////////
//                   ALL STUDENTS COMPLETE THESE SECTIONS
// Main Class File:  TheGame.java
// File:             Item.java
// Semester:         CS367 Fall 2015
//
// Author:           Huilin Hu
// CS Login:         huilin
// Lecturer's Name:  Jim Skrentny
// Section:      	 Lecture 2
//
//////////////////// PAIR PROGRAMMERS COMPLETE THIS SECTION ////////////////////
//
// Pair Partner:     Weilan Yang
// Email:            wyang65@wisc.edu
// CS Login:         wyang
// Lecturer's Name:  Jim Skrentny
// Section:      	 Lecture 2
//
//////////////////////////// 80 columns wide //////////////////////////////////

/**
 * The Item class defines an item that a player can interact with
 * @author Huilin Hu
 *
 */
public class Item {
	// item name
	private String	name;
	// item description
	private String	description;
	// activation status
	private boolean activated;
	// the message an item sends when used
	private String message;
	// if an item is for one time use or not
	private boolean oneTimeUse;
	// the "on_useString" for the item
	private String usedString;
	
	/**
	 * Constructs an item that do specific things to a room.
	 * @param name Item name
	 * @param description Item description
	 * @param activated Activation status
	 * @param message Message an item sends when used
	 * @param oneTimeUse An item is for one time use or not
	 * @param usedString The string to be printed when used
	 */
	public Item(String name, String description, boolean activated, 
			String message,boolean oneTimeUse, String usedString){
    	if (name == null || name.equals("") || description == null || 
    		description.equals("") || message == null ||
    		message.equals("") || usedString == null || usedString.equals(""))
    		throw new IllegalArgumentException();
    	
    	this.name = name;
    	this.description = description;
    	this.activated = activated;
    	this.message = message;
    	this.oneTimeUse = oneTimeUse;
    	this.usedString = usedString;
	}
	
	/**
	 * Return item name
	 * @return Item name
	 */
	public String getName(){
    	return name;
	}
	
	/**
	 * Return description of the Item
	 * @return Description of the Item
	 */
	public String getDescription(){
    	return description;
	}
	
	/**
	 * Checks if the item is activated. 
	 * @return True if it is activated, else return false.
	 */
	public boolean activated(){
    	return activated;
	}
	
	/**
	 * Returns the "message" that the item wants to send to the room. 
	 * @return The "message" that the item wants to send to the room. 
	 */
	public String on_use(){
    	return message;
	}
	
	/**
	 * Activates the object. 
	 */
	public void activate(){
    	if (activated == false)
    		activated = true;
    }
	
	/**
	 * Returns the "on_useString" for the Item. This is printed in the notifyRoom() 
	 * function in TheGame class after an item has been used and is active.
	 * @return The "on_useString" for the Item
	 */
	public String on_useString(){
    	return usedString;
	}
	
	/**
	 * Returns true if the item can only be used once. Else returns false. 
	 * @return True if the item can only be used once. Else returns false. 
	 */
	public boolean isOneTimeUse(){
    	return oneTimeUse;
	}

	@Override
	//This returns a String consisting of the name and description of the Item
	//This has been done for you.
	//DO NOT MODIFY
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("Item Name: " + this.name);
		sb.append(System.getProperty("line.separator"));
		sb.append("Description: " + this.description);
		return sb.toString();
	}
}
