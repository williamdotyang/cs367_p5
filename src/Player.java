///////////////////////////////////////////////////////////////////////////////
//                   ALL STUDENTS COMPLETE THESE SECTIONS
// Main Class File:  TheGame.java
// File:             Player.java
// Semester:         CS367 Fall 2015
//
// Author:           Huilin Hu
// CS Login:         huilin
// Lecturer's Name:  Jim Skrentny
// Section:      	 Lecture 2
//
//////////////////// PAIR PROGRAMMERS COMPLETE THIS SECTION ////////////////////
//
// Pair Partner:     Weilan Yang
// Email:            wyang65@wisc.edu
// CS Login:         wyang
// Lecturer's Name:  Jim Skrentny
// Section:      	 Lecture 2
//
//////////////////////////// 80 columns wide //////////////////////////////////
import java.util.Set;
import java.util.Iterator;
import java.util.HashSet;

/**
 * The player class defines a player who participates in the game
 * @author Huilin Hu
 *
 */
public class Player {
	// player name
	private String name;
	// the magic sack held by the player that contains all his/her items
	private Set<Item> magicSack;
	//Do not add anymore private data members

	/**
	 * Constructs a player with his/her name and starting items.
	 * @param name Player's name
	 * @param startingItems Items that a player has when the game starts
	 */
	public Player(String name, Set<Item> startingItems){
		if (name == null || name.equals("") || startingItems == null)
			throw new IllegalArgumentException();
		this.name = name;
		this.magicSack = startingItems;
	}
	
	/**
	 * Return the name of player
	 * @return the name of player
	 */
	public String getName(){
    	return name;
	}
	
	//Returns a String consisting of the items in the sack
	//DO NOT MODIFY THIS METHOD
	public String printSack(){
		//neatly printed items in sack
		StringBuilder sb = new StringBuilder();
		sb.append("Scanning contents of your magic sack");
		sb.append(System.getProperty("line.separator"));
		for(Item itm : magicSack){
			sb.append(itm.getName());
			sb.append(System.getProperty("line.separator"));
		}
		return sb.toString();
	}
	
	/**
	 * Iterate through the sack, and find the items whose status is activated.
	 * @return the set that contains all activated items
	 */
	public Set<Item> getActiveItems(){
		Set <Item> activeItems = new HashSet<Item> ();
    	Iterator<Item> itr = magicSack.iterator();
    	while (itr.hasNext()) {
    		Item newItem = itr.next();
    		if (newItem.activated())
    			activeItems.add(newItem);
    	}
    	return activeItems;
	}
	
	/**
	 * Find the Item in the sack by its name. 
	 * 
	 * @param item The name of the item
	 * @return Return the item if it is found, otherwise return null.
	 */
	public Item findItem(String item){
		if (item == null)
			throw new IllegalArgumentException();
    	Iterator<Item> itr = magicSack.iterator();
    	while (itr.hasNext()) {
    		Item newItem = itr.next();
    	    if (newItem.getName().equals(item))
    	    	return newItem;
    	}
    	return null;
	}
	 
	/**
	 * Checks if the player has the "item" in his sack. 
	 * @param item The item object to be checked.
	 * @return Returns true if the item is found, otherwise returns false.
	 */
	public boolean hasItem(Item item){
		if (item == null)
			throw new IllegalArgumentException();
    	Iterator<Item> itr = magicSack.iterator();
    	while (itr.hasNext()) {
    		Item newItem = itr.next();
    	    if (newItem.equals(item))
    	    	return true;
    	}
    	return false;
	}
	
	/**
	 * Adds the "item" to the Player's sack. Duplicate items are not allowed. 	
	 * @param item The item object to be added.
	 * @return Returns true if item successfully added, else returns false
	 */
	public boolean addItem(Item item){
    	if (item == null)
    		throw new IllegalArgumentException();
    	return magicSack.add(item);
	}
	
	/**
	 * Removes the "item" from the sack. 
	 * @param item The item object to be removed.
	 * @return Returns true if removal is successful, else returns false.
	 */
	public boolean removeItem(Item item){
    	if (item == null)
    		throw new IllegalArgumentException();
    	return magicSack.remove(item);
	}
}
