///////////////////////////////////////////////////////////////////////////////
//                   ALL STUDENTS COMPLETE THESE SECTIONS
// Main Class File:  TheGame.java
// File:             DirectedGraph.java
// Semester:         CS367 Fall 2015
//
// Author:           Weilan Yang, wyang65@wisc.edu
// CS Login:         wyang
// Lecturer's Name:  Jim Skrentny
// Section:      	 Lecture 2
//
//////////////////// PAIR PROGRAMMERS COMPLETE THIS SECTION ////////////////////
//
// Pair Partner:     Huilin Hu
// Email:            hhu28@wisc.edu
// CS Login:         huilin
// Lecturer's Name:  Jim Skrentny
// Section:      	 Lecture 2
//
//////////////////////////// 80 columns wide //////////////////////////////////
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Implements GraphADT with an adjacency lists representation. The graph has 
 * no loops, i.e., no vertex has an edge to itself. Each edge will be a 
 * directed edge.
 *
 * @author Weilan Yang
 */
public class DirectedGraph<V> implements GraphADT<V>{
	// fields
	private HashMap<V, ArrayList<V>> hashmap;
    //DO NOT ADD ANY OTHER DATA MEMBERS

	// constructors
	/** Creates an empty graph. */
    public DirectedGraph() {
    	hashmap = new HashMap<V, ArrayList<V>>();
    }

    /** Creates a graph from a preconstructed hashmap. */ 
    public DirectedGraph(HashMap<V, ArrayList<V>> hashmap) {
    	if (hashmap == null) 
    		throw new IllegalArgumentException("hashmap cannot be null!");
    	
    	this.hashmap = hashmap;
    }

    // methods
    /** Adds the specified vertex to this graph if not already present. More 
	 * formally, adds the specified vertex v to this graph if this graph 
	 * contains no vertex u such that u.equals(v). If this graph already 
	 * contains such vertex, the call leaves this graph unchanged and returns false.
	 * @param vertex
	 * @return true if added, false otherwise
	 */
    @Override
    public boolean addVertex(V vertex) {
    	if (vertex == null) 
    		throw new IllegalArgumentException("vetex cannot be null!");
    	
    	if (hashmap.containsKey(vertex)) return false;
    	hashmap.put(vertex, new ArrayList<V>());
    	return true;
    }

    /**
     * Creates a new edge from vertex v1 to v2 and returns true, 
     * if v1.equals(v2) evaluates to false and an edge does not already exist 
     * from v1 and v2. Returns false otherwise. 
 	 * Vertices v1 and v2 must already exist in this graph. If they are not 
 	 * found in the graph IllegalArgumentException is thrown.
     * @param v1 
     * @param v2
     * @return true if added, false otherwise
     */
    @Override
    public boolean addEdge(V v1, V v2) {
    	if (v1 == null || v2 == null)
    		throw new IllegalArgumentException("vertices cannot be null!");
    	
    	if (!hashmap.containsKey(v1)) throw new IllegalArgumentException(
    			"vertex " + v1 + " doesn't exists in graph!");
    	
    	if (!hashmap.containsKey(v2)) throw new IllegalArgumentException(
    			"vertex " + v2 + " doesn't exists in graph!");
    	
    	ArrayList<V> v1List = hashmap.get(v1);
    	if (!v1.equals(v2) && !v1List.contains(v2)) { 
    		v1List.add(v2);
    		return true;
    	} else { 
    		return false;
    	}
    }

    /**
     * Returns a set of all vertices to which v has outward edges. 
 	 * Vertex v must already exist in this graph. If it is not found in the  
 	 * graph IllegalArgumentException is thrown
     * @param vertex
     * @return a set of all vertices to which v has outward edges
     */
    @Override
    public Set<V> getNeighbors(V vertex) {
    	if (vertex == null) 
    		throw new IllegalArgumentException("vetex cannot be null!");
    	
    	if (!hashmap.containsKey(vertex)) throw new IllegalArgumentException(
    				"vertex " + vertex + " doesn't exists in graph!");
    	
    	Set<V> neighbors = new HashSet<V>(hashmap.get(vertex));
    	return neighbors;
    }

    /**	
     * If both v1 and v2 exist in the graph, and an edge exists from v1 to v2, 
     * remove the edge from this graph. Otherwise, do nothing.
     * @param v1
     * @param v2
     */
    @Override
    public void removeEdge(V v1, V v2) {
    	if (v1 == null || v2 == null)
    		throw new IllegalArgumentException("vertices cannot be null!");
    	
    	if (hashmap.containsKey(v1) && hashmap.containsKey(v1)) 
    		hashmap.get(v1).remove(v2);
    }

    /**
     * Returns a set of all the vertices in the graph.
     * @return a set of all the vertices in the graph
     */
    @Override
    public Set<V> getAllVertices() {
    	return new HashSet<V>(hashmap.keySet());
    }

    @Override
    //Returns a String that depicts the Structure of the Graph
    //This prints the adjacency list
    //This has been done for you
    //DO NOT MODIFY
    public String toString() {
        StringWriter writer = new StringWriter();
        for (V vertex: this.hashmap.keySet()) {
            writer.append(vertex + " -> " + hashmap.get(vertex) + "\n");
        }
        return writer.toString();
    }

}
